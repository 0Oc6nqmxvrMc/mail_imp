******************************************************************************
**		                   PATH IMPRESS�O TFP	                    **
******************************************************************************
#DEFINE DIRSERVIMP		"\\ISIDORO\SERVIMP\"
#DEFINE DIRDATA			"\\ISIDORO\SERVIMP\SRAINHA_IMP\DATA\"
#DEFINE DIRDIARIO		"\\ISIDORO\SERVIMP\SRAINHA_IMP\DIARIO\"
#DEFINE DIRRELAT		"\\ISIDORO\SERVIMP\SRAINHA_IMP\DIARIO\RELATORIOS\"
#DEFINE DIRSPROGS		"\\ISIDORO\SERVIMP\SRAINHA_IMP\PROGS\"
#DEFINE DIRDOCS  		"\\ISIDORO\SERVIMP\SRAINHA_IMP\DOCS\"
#DEFINE DIRIMPRESSAO	"\\SKY\IMPRESSAO\01_IMPRIMIR\"
#DEFINE	DIRARQS			"\\ISIDORO\SERVIMP\SRAINHA_IMP\ARQ_DIA\"
#DEFINE	DIRARQSPED		"\\ISIDORO\SERVIMP\SRAINHA_IMP\ARQ_DIA\PEDIDOS\"
#DEFINE	DIRARQSCART		"\\ISIDORO\SERVIMP\SRAINHA_IMP\ARQ_DIA\CARTAS\"
#DEFINE DIRERROS		"\\ISIDORO\SERVIMP\SRAINHA_IMP\ERR\"

#DEFINE TTAB			"\\ISIDORO\SERVIMP\SRAINHA_IMP\DATA\TAB_REG"
#DEFINE TTABE13			"\\ISIDORO\SERVIMP\SRAINHA_IMP\DATA\TAB_REGE13"
#DEFINE TCART			"\\ISIDORO\SERVIMP\SRAINHA_IMP\DATA\TAB_CART"
#DEFINE TPED			"\\ISIDORO\SERVIMP\SRAINHA_IMP\DATA\TAB_PED"

#DEFINE DIREXE			"\\ISIDORO\SERVIMP\SRAINHA_IMP\PROGS\MAIL_IMP_001.EXE"

#DEFINE DIRIMPRESSAO	        "\\ISIDORO\SERVIMP\IMPRESSAO\"

*****************************************************************************
**		PATH SCLEMENTE						   **
*****************************************************************************
*!*	#DEFINE DIRSERVIMP		"C:\SERVIMP\"
*!*	#DEFINE DIRDATA			"C:\SERVIMP\SRAINHA\DATA\"
*!*	#DEFINE DIRDIARIO		"C:\SERVIMP\SRAINHA\DIARIO\"
*!*	#DEFINE DIRRELAT		"C:\SERVIMP\SRAINHA\DIARIO\RELATORIOS\"
*!*	#DEFINE DIRSPROGS	 	"C:\SERVIMP\SRAINHA\PROGS\"
*!*	#DEFINE DIRIMPRESSAO	        "C:\SERVIMP\IMPRESSAO\"
*!*	#DEFINE	DIRARQS			"C:\SERVIMP\SRAINHA\ARQ_DIA\"
*!*	#DEFINE	DIRARQSPED		"C:\SERVIMP\SRAINHA\ARQ_DIA\PEDIDOS\"
*!*	#DEFINE	DIRARQSCART		"C:\SERVIMP\SRAINHA\ARQ_DIA\CARTAS\"
*!*	#DEFINE DIRERROS		"C:\SERVIMP\SRAINHA\ERR\"

*!*	#DEFINE TTAB			"C:\SERVIMP\SRAINHA\DATA\TAB_REG"
*!*	#DEFINE TTABE13			"C:\SERVIMP\SRAINHA\DATA\TAB_REGE13"
*!*	*#DEFINE TCART			"C:\SERVIMP\SRAINHA\DATA\TAB_CART"
*!*	#DEFINE TCART			"\\GABRIEL\IMPRESSAO\DIVERSOS\TAB_CART"
*!*	*#DEFINE TPED			"C:\SERVIMP\SRAINHA\DATA\TAB_PED"
*!*	#DEFINE TPED			"\\GABRIEL\IMPRESSAO\DIVERSOS\TAB_PED"

*!*	#DEFINE DIRREPORTS		"L:\PROJETOS\\MAIL\REPORTS\"
*!*	#DEFINE DIRGRAFICOS		"L:\PROJETOS\\MAIL\GRAPHICS\"
*!*	#DEFINE DIRPROGS		"L:\PROJETOS\\MAIL\PROGS\"
*!*	#DEFINE DIRFORMS		"L:\PROJETOS\\MAIL\FORMS\"
*!*	#DEFINE DIRRAIZ			"L:\PROJETOS\\MAIL\"

*!*	#DEFINE DIREXE			"L:\PROJETOS\MAIL\MAIL.EXE"