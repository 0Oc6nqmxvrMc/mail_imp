#INCLUDE PATH.H
SET EXCLUSIVE OFF 
SET DATE BRITISH 
SET CENTURY ON 
SET CONFIRM OFF
SET SAFETY OFF 
SET DELETED OFF
*SET RESOURCE OFF

*!*	PUBLIC cDrt,cDrv,cPrg,cPth,cPthSRainha

*!*	cDrt        = SYS(2003) && Diretorio atual
*!*	cDrv        = SYS(5)    && Drive atual
*!*	cPrg        = SYS(16)   && Programa em execu��o
*!*	cPth        = LEFT(cPrg,RAT("\", cPrg, 1)) && Programa iniciado em "cPth"
*!*	cPthSRainha = LEFT(cPrg,RAT("\", cPrg, 2)) && Raiz do SRainha

SET PATH TO DATA;FORMS;GRAPHICS;PROGS;REPORTS;CLASSES;BMPS
SET LIBRARY TO [foxtools.fll] ADDITIVE
SET PROCEDURE TO [bcfont.prg],[cepnet.prg] ADDITIVE


ON ERROR do controla_erro WITH  ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( )

_SCREEN.CAPTION     = [Salvai-me Rainha de F�tima]
_SCREEN.ICON        = [/bmps/brasil.ico]
_SCREEN.WINDOWSTATE = 2
_SCREEN.CONTROLBOX  = .T.
_SCREEN.MINBUTTON   = .T.
_SCREEN.MAXBUTTON   = .F.
_SCREEN.CLOSABLE    = .F.

DO FORM frmaber_imp.scx
DO impress.mpr

READ EVENTS
