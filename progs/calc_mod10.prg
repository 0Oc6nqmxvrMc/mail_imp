***********************************************************************
* Rotina de c�lculo dos d�gitos verificadores da cobran�a s/registro 
* 
***********************************************************************
*
***************************************************************************
* C�lculo do d�gito verificador dos campos (V_TIPO_COB+V_CCORRENTE)
* M�DULO 10
***************************************************************************
*
PARAMETERS PARA_DAC_NOSSONUM
LOCAL V_COB_NOSSONUM, V_DAC,ACUMULADOR0,MULTIPLICADOR,ACUMULADOR1,NCARCAMPO1
V_COB_NOSSONUM = PARA_DAC_NOSSONUM
	IF PARAMETERS() = 1

		vCobReciboCCorrente = V_COB_NOSSONUM
		multiplicador = 2
		acumulador0   = 0
		acumulador1   = 0
		nCarCampo1    = len(vCobReciboCCorrente)
		*
		for d = nCarCampo1 to 1 step -1
			v_dv0 = val(subs(vCobReciboCCorrente,d,1)) * multiplicador
			if v_dv0 > 9
				v_dv0 = val(subs(ltrim(str(v_dv0)),1,1)) + val(subs(ltrim(str(v_dv0)),2,1))
			endif
			acumulador0   = acumulador0 + v_dv0
			multiplicador = iif(multiplicador=1,2,1)
		next
		*
		v_divisao0 = mod(acumulador0, 10)
		*
		if v_divisao0 = 0
			v_dv0 = 0
		else
			v_dv0 = 10 - v_divisao0
		endif
		*
		dv0 = alltr(str(v_dv0))
		V_DAC = dv0
		RETURN V_DAC
	ENDIF
RETURN ""
*
***********************************************************************
*
