*
*************************************
* Titulo     : Personal.PRG
* Autor      : Claudinei Maria
* Data       : 21/03/1998
*************************************
* Alterado em 11/06/2010
* CNOME   = IIF( LNOME_N, IIF( SUBPREPOS $ PREPOSICOES, LEFT( PNOME, IIF(AT3=0,LENNOME,AT3)),;
* AT3 = 0 // Corre��o para vari�veis que n�o tem espa�o ap�s a terceira part�cula do nome
* Ex: "JOSE DE KASHI" X "JOSE DE KASHI "
*************************************
PARAMETERS _NOME
*
LOCAL CNOME, LNOME_N, LNOME_P, PNOME, PREPOSICOES, EXNOMES, AT1, AT2, AT3
LOCAL LENNOME, LEFTNOME, SUBPREPOS

PREPOSICOES = [" DO "," DOS "," DA "," DAS "," DE "," DEL "]
EXNOMES     = "ANA,ANNA,JOAO,JOSE,MARIA"

SET TALK OFF
	PNOME     = _NOME
	CNOME     = ""
	LNOME_N   = .F.
	LNOME_P   = .F.
	
	AT1       =  AT( " ", PNOME )
	AT2       =  AT( " ", PNOME, 2 )
	AT3       =  AT( " ", PNOME, 3 )

	LENNOME   = LEN( PNOME )
	LEFTNOME  = LEFT( PNOME, AT1 - 1 )
	SUBPREPOS = SUBS( PNOME, AT1, ( AT2 - AT1 ) )

	
	CNOME   = LEFTNOME
	LNOME_N = IIF( CNOME $ EXNOMES, .T., .F. )
	CNOME   = IIF( LNOME_N, IIF( SUBPREPOS $ PREPOSICOES, LEFT( PNOME, IIF(AT3=0,LENNOME,AT3)),;
				 LEFT( PNOME, IIF(AT2=0,LENNOME,AT2))), CNOME )
	CNOME   = ALLTRIM( CNOME )
	DO CASE
		CASE PNOME = [EDER JOSE] AND CNOME = [EDER]
			CNOME = [EDER JOSE]
		
		CASE PNOME = [VERA LUCIA] AND CNOME = [VERA]
			CNOME = [VERA LUCIA]
	ENDCASE
RETURN CNOME
