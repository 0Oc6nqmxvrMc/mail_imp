FUNCTION cepnet
PARAMETERS lcep
private lcep

IF PARAMETERS() = 1
	IF LEN(alltrim(lcep)) = 8
		nSoma = 0
		FOR i=8 TO 1 STEP -1
			nSoma = nSoma + VAL(SUBSTR(lCep,i,1))
		NEXT
		return "/" + Lcep+ iif(nSoma % 10 > 0, str(10 - nSoma % 10,1), "0") + "\" 
	ELSE
		MESSAGEBOX('N�mero de d�gitos do CEP � inv�lido: ' + lcep + CHR(13) ;
		  + 'Caso repita este erro muitas vezes dentro do mesmo arquivo, ' + ;
		  'favor avisar o administrador.       ',48,[Alerta!])
	ENDIF
ENDIF
return "      "
*****************