PARAMETER merror, mess, mess1, mprog, mlineno
#include path.h
#DEFINE FILENOTFOUND_LOC			"Arquivo ou vers�o do programa n�o encontrado!!!   "
#DEFINE FILEVER_FILEVER_LOC 		"Vers�o: "
#DEFINE FILEVER_INTERNAL_LOC		"Nome: "
#DEFINE FILEVER_FILENAME_LOC	 	"Nome Original do Execut�vel: "
#DEFINE FILEVER_NOVERSION_LOC		"Informa��o da vers�o n�o encontrada."
*****
=MESSAGEBOX('Houve um erro, imposs�vel continuar!' + chr(13) + ;
		'O administrador ser� comunicado automaticamente, aguarde!!!      ' + chr(13) + chr(13) + ;
 		'MENSAGEM DO ERRO:  ' + mess + chr(13) + ;
	    'PROGRAMA COM ERRO:  ' + mprog + chr(13) + ;
	    'LINHA DO PROGRAMA:  ' + alltrim(str(mlineno)) + chr(13) + ;
 		'N�MERO DE ERRO:  ' + LTRIM(STR(merror)),16,[Aten��o!])
*****
thestring = ""
NotFound = ""
tt = (DIREXE)
IF FILE (tt)
	DIMENSION averarray(1)
	ntheval = aGetFileVersion(averarray,tt)
	IF ntheval # 0

		IF NOT EMPTY(averarray(4))
			thestring = thestring + FILEVER_FILEVER_LOC + ALLT(averarray(4))
			
		ENDIF
		IF NOT EMPTY(averarray(5))
			thestring = thestring + CHR(13) + FILEVER_INTERNAL_LOC+ ALLT(averarray(5))
			
		ENDIF
		
		IF NOT EMPTY(averarray(8))
			thestring = thestring+chr(13)+FILEVER_FILENAME_LOC+ ALLT(averarray(8))
		ENDIF
		
		IF EMPTY(thestring)
			thestring =  FILEVER_NOVERSION_LOC + tt 
			
		ENDIF
	ELSE
		thestring =  FILEVER_NOVERSION_LOC + tt
			
	ENDIF
ELSE
*	NotFound = messagebox(FILENOTFOUND_LOC,64,[Informa��o!],1)
	NotFound = FILENOTFOUND_LOC
ENDIF
editverinfo = thestring
*messagebox(editverinfo,64,[Informa��o!])
*****
crLf= CHR(13)+ CHR(10)
logerros = (DIRERROS + SYS(3)+ ".err")
	
	hcLog= fcreate(logerros)
	logStr = crLf + [************ Erro *************]
	=fwrite(hcLog, logStr)
	=fwrite(hcLog, crLf)

	logStr = 'Error message: ' + mess
	=fwrite(hcLog, logStr)
	=fwrite(hcLog, crLf)

	logStr = 'Program with error: ' + mprog
	=fwrite(hcLog, logStr)
	=fwrite(hcLog, crLf)

	logStr ='Line number of error: ' + LTRIM(STR(mlineno)) 
	=fwrite(hcLog, logStr)
	=fwrite(hcLog, crLf)

	logStr= 'Error number: ' + LTRIM(STR(merror))
	=fwrite(hcLog, logStr)
	=fwrite(hcLog, crLf)

*!*		logStr = 'Line of code with error: ' + mess1
*!*		=fwrite(hcLog, logStr)
*!*		=fwrite(hcLog, crLf)

	logStr = [Tabela: ] + dbf() + [     ] + [Registro: ] + ALLTRIM(str(recno())) + crLf
	=fwrite(hcLog, logStr)
	=fwrite(hcLog, crLf)

IF ! EMPTY(editverinfo)
		logStr = [Informa��es do Programa:] + crLf + editverinfo
		=fwrite(hcLog, logStr)
		=fwrite(hcLog, crLf)
		=fclose(hcLog)
	ELSE
		logStr = FILENOTFOUND_LOC  + crLf
		=fwrite(hcLog, logStr)
		=fwrite(hcLog, crLf)	
		=fclose(hcLog)
		set safety off
		list memory to file (logerros) additive noconsole
		list status to (logerros) additive noconsole
ENDIF

*!*			IF ! EMPTY(NotFound)
*!*					logStr = FILENOTFOUND_LOC  + crLf
*!*					=fwrite(hcLog, logStr)
*!*					=fwrite(hcLog, crLf)	
*!*					=fclose(hcLog)
*!*					set safety off
*!*					list memory to file (logerros) additive noconsole
*!*					list status to (logerros) additive noconsole
*!*				ENDIF

set safety on
close all
CANCEL && Joga o usu�rio fora, evitando impressoes com erro no programa
return
***************************************************