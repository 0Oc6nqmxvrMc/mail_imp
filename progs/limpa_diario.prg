#include path.h

WAIT WINDOW [Apagando Arquivos...] nowait
CLOSE ALL
SET DEFAULT TO (DIRDIARIO)

dt1 = [ZZ] + RIGHT(DTOS(DATE()),6) + [.DBF]
dt2 = [ZB] + RIGHT(DTOS(DATE()),6) + [P] + [.DBF]
dt3 = [ZP] + RIGHT(DTOS(DATE()),6) + [.DBF]

Narray = ADIR(dbfs,[*.dbf])

PUBLIC ARRAY Varqs(Narray,1)
Ncount = 1 
DO WHILE Ncount <= Narray 
	Varqs[Ncount,1] = DBFS[Ncount,1]
	rm = ALLTRIM(Varqs[Ncount,1])
	IF rm # dt1 AND rm # dt2 AND rm # dt3
		DELETE FILE (DIRDIARIO + rm)
	ENDIF
	Ncount = Ncount + 1
ENDDO	
ERASE *.bak
ERASE *.BAK
WAIT WINDOW [Todos arquivos anteriores ao dia de hoje, foram apagados!] TIMEOUT 1.5