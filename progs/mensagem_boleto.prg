Parameters pbanco,pcampo,psigla_assoc
Local lbanco,lcampo,lTexto,lsigla_assoc
lbanco = pbanco
lcampo = pcampo
If Parameters() = 3
	lsigla_assoc = Alltrim(psigla_assoc)
Endif
Do Case

	Case lbanco = "001"
		If lcampo = "DS_BANCO"
			lTexto = "BANCO DO BRASIL"
		Endif

		If lcampo = "CARTEIRA"
			If Substr(linha1,43,2) = "17"
				lTexto = "17/01-9"
			Else
				lTexto = "18-035"
			Endif
		Endif

		If lcampo = "BANCO_DIG"
			lTexto = "001-9"
		Endif

		If lcampo = "AGENCIA_CODIGO_CEDENTE"
			lTexto = agencia+'-'+d_agencia+'/'+Allt(Str(Val(cc)))+'-'+d_cc
		Endif

		If lcampo = "NOSSO_NUMERO"
			lTexto = nosso_num
		Endif

		If lcampo = "VALORDOC"
			lTexto = Alltr(Str(valordoc,14,2))
		Endif

		If lcampo = "ESPECIE_DOC"
			If Substr(linha1,43,2) = "17"
				lTexto = "DM"
			Else
				lTexto = "RC"
			Endif

		Endif

		If lcampo = "ESPECIE_MOEDA"
			lTexto = "R$"
		Endif

		If lcampo = "ACEITE"
			lTexto = "N"
		Endif

		If lcampo = "CARTEIRA_NOSSO_NUMERO"
			lTexto = Substr(linha1,43,2)+"/"+Substr(linha1,26,17)
		Endif

		If lcampo = "SITE_BANCO"
			lTexto = "www.bb.com.br"
		Endif

		If lcampo = "INSTR_PAG"
			lTexto = "Banco do Brasil"
		Endif
	Case lbanco = "104"
		If lcampo = "DS_BANCO"
			lTexto = "CAIXA ECONOMICA"
		Endif

		If lcampo = "CARTEIRA"
			lTexto = "SR"
		Endif

		If lcampo = "BANCO_DIG"
			lTexto = "104-0"
		Endif

		If lcampo = "AGENCIA_CODIGO_CEDENTE"
			lTexto = ag_codced
		Endif

		If lcampo = "NOSSO_NUMERO"
			lTexto = nosso_num
		Endif

		If lcampo = "VALORDOC"
			lTexto = valordoc
		Endif

		If lcampo = "ESPECIE_DOC"
			lTexto = "OU"
		Endif

		If lcampo = "ESPECIE_MOEDA"
			lTexto = "R$"
		Endif

		If lcampo = "ACEITE"
			lTexto = "N"
		Endif

		If lcampo = "CARTEIRA_NOSSO_NUMERO"
			lTexto = ""
		Endif
		If lcampo = "SITE_BANCO"
			lTexto = "www.caixa.com.br"
		Endif

		If lcampo = "INSTR_PAG"
			lTexto = "Caixa Econ�mica Federal"
		Endif

	Case lbanco = "237"
		If lcampo = "DS_BANCO"
			lTexto = "BRADESCO"
		Endif

		If lcampo = "CARTEIRA"
			lTexto = "06"
		Endif

		If lcampo = "BANCO_DIG"
			lTexto = "237-2"
		Endif

		If lcampo = "AGENCIA_CODIGO_CEDENTE"
			lTexto = agencia+'-'+d_agencia+'/'+Transform(cc)+'-'+d_cc
		Endif

		If lcampo = "NOSSO_NUMERO"
			lTexto = "06/"+nosso_num+"-"+dv_ns
		Endif

		If lcampo = "VALORDOC"
			lTexto = Alltr(Str(valordoc,14,2))
		Endif

		If lcampo = "ESPECIE_DOC"
			lTexto = "DM"
		Endif

		If lcampo = "ESPECIE_MOEDA"
			lTexto = "R$"
		Endif

		If lcampo = "ACEITE"
			lTexto = "N�o"
		Endif

		If lcampo = "CARTEIRA_NOSSO_NUMERO"
			lTexto = ""
		Endif

		If lcampo = "SITE_BANCO"
			lTexto = "www.bradesco.com.br"
		Endif

		If lcampo = "INSTR_PAG"
			lTexto = "Banco Bradesco"
		Endif

	Case lbanco = "341"
		If lcampo = "DS_BANCO"
			lTexto = "BANCO ITA�"
		Endif

		If lcampo = "CARTEIRA"
			lTexto = Substr(linha1,20,3)
		Endif

		If lcampo = "BANCO_DIG"
			lTexto = "341-7"
		Endif

		If lcampo = "AGENCIA_CODIGO_CEDENTE"
			lTexto = Padl(Alltr(agencia),4,"0")+'/'+Right(cc,5)+'-'+d_cc
		Endif

		If lcampo = "NOSSO_NUMERO"
			Do Case
				Case Substr(linha1,20,3) = "198"
					lTexto = Right(Right(nossonum,15),7)

				Case Substr(linha1,20,3) = "175"
					lTexto = Substr(linha1,20,3) + "/"+Substr(linha1,23,8) + "-"+Substr(linha1,31,1)
			Endcase
		Endif

		If lcampo = "CARTEIRA_NOSSO_NUMERO"
**lTexto = SUBSTR(linha1,20,3) + "/"+ left(RIGHT(ALLTRIM(nossonum),15),8)+ "-"+calc_mod10(AGENCIA+LEFT(CC,5)+SUBSTR(linha1,20,3) +left(RIGHT(ALLTRIM(nossonum),15),8))
			lTexto = Substr(linha1,20,3) + "/"+Substr(linha1,23,8) + "-"+Substr(linha1,31,1)
		Endif

		If lcampo = "VALORDOC"
			lTexto = Alltr(Str(valordoc,10,2))
		Endif

		If lcampo = "ESPECIE_DOC"
			lTexto = "DP"
		Endif

		If lcampo = "ESPECIE_MOEDA"
			lTexto = "R$"
		Endif

		If lcampo = "ACEITE"
			lTexto = "N"
		Endif

		If lcampo = "SITE_BANCO"
			lTexto = "www.itau.com.br"
		Endif

		If lcampo = "INSTR_PAG"
			lTexto = "Banco Ita�"
		Endif
	Case lbanco = "033"
		If lcampo = "DS_BANCO"
			lTexto = "SANTANDER"
		Endif

*!*			IF lcampo = "CARTEIRA"
*!*				lTexto = "102"
*!*			ENDIF

		If lcampo = "CARTEIRA"
			lTexto = "101"
		Endif

		If lcampo = "BANCO_DIG"
			lTexto = "033-7"
		Endif

		If lcampo = "AGENCIA_CODIGO_CEDENTE"
			lTexto = agencia +[/] + cod_ced
		Endif

		If lcampo = "NOSSO_NUMERO"
			lTexto = Alltr(nossonum)
		Endif

		If lcampo = "VALORDOC"
			lTexto = valordoc
		Endif

		If lcampo = "ESPECIE_DOC"
			lTexto = "DM"
		Endif

		If lcampo = "ESPECIE_MOEDA"
			lTexto = "R$"
		Endif

		If lcampo = "ACEITE"
			lTexto = "N"
		Endif

*!*			IF lcampo = "CARTEIRA_NOSSO_NUMERO"
*!*						lTexto = STUFF(right(nossonum,13),13,0,[ ])
*!*			ENDIF

		If lcampo = "CARTEIRA_NOSSO_NUMERO"
			lTexto = ""
		Endif

		If lcampo = "SITE_BANCO"
			lTexto = "www.santander.com.br"
		Endif

		If lcampo = "INSTR_PAG"
			lTexto = "Banco Santander"
		Endif

	Otherwise
		If lcampo = "INSTRUCOES"
			Do Case
				Case lsigla_assoc = "SBDTFP"
					TEXT TO lTexto NOSHOW PRETEXT 2
					Pag�vel em qualquer Banco ou Casas Lot�ricas
					TRATA-SE DE DOA��O - N�O COBRAR JUROS DE MORA
					ENDTEXT
			Endcase
		Endif

		If lcampo = "CEDENTE"
			Do Case
				Case lsigla_assoc = "SBDTFP"
					lTexto = "Campanha Vinde Nossa Senhora de F�tima, n�o Tardeis! - (SBDTFP) - CNPJ: 60.758.505/0001-41"

			Endcase
		Endif

Endcase

Return lTexto
