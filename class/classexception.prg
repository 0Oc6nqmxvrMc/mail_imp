Define Class ClassException As Custom
   Protected oNomePrograma           As String

   Protected Procedure Init As void
      This.oNomePrograma = ""
   Endproc

   Procedure GetException As String
      Parameters rException As Exception, rNomePrograma As String
      Local lMsg As String, lNomePrograma As String
      Local lException As Exception

      This.oNomePrograma = Iif( Vartype( rNomePrograma ) # "C", "", rNomePrograma )

      Try
         lMsg = [Mensagem de erro no programa: ] +  This.oNomePrograma + Chr(13) + Chr(10) + ;
            [Linha: ] + Alltrim(Str(rException.Lineno)) + Chr(13) + Chr(10) + ;
            [Erro: ] + Alltrim(Str(rException.ErrorNo)) + Chr(13) + Chr(10) + ;
            [Mensagem: ] + rException.Message  + Chr(13) + Chr(10) + ;
            [Procedure: ] + rException.Procedure + Chr(13) + Chr(10) + ;
            [Detalhes: ] + rException.Procedure + Chr(13) + Chr(10) + ;
            [StackLevel: ] + Alltrim(Str(rException.StackLevel)) + Chr(13) + Chr(10) + ;
            [Conte�do Linha: ] + rException.LineContents + Chr(13) + Chr(10)

      Catch To lException
         lMsg = [Mensagem de erro no programa ClassException: ] + Chr(13) + Chr(10) + ;
            [Linha: ] + Alltrim(Str(lException.Lineno)) + Chr(13) + Chr(10) + ;
            [Erro: ] + Alltrim(Str(lException.ErrorNo)) + Chr(13) + Chr(10) + ;
            [Mensagem: ] + lException.Message  + Chr(13) + Chr(10) + ;
            [Procedure: ] + lException.Procedure + Chr(13) + Chr(10) + ;
            [Detalhes: ] + lException.Procedure + Chr(13) + Chr(10) + ;
            [StackLevel: ] + Alltrim(Str(lException.StackLevel)) + Chr(13) + Chr(10) + ;
            [Conte�do Linha: ] + lException.LineContents + Chr(13) + Chr(10)

      Endtry

      Return lMsg
   Endproc

   Protected Procedure Destroy As void
      This.Destroy()
   Endproc
Enddefine
